#include <cuda_runtime.h>

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>

void checkCudaError(cudaError_t error)
{
    if (error != cudaSuccess)
    {
        fprintf(stderr, "Cuda Fail: (error code %s)\n", cudaGetErrorString(error));
        exit(-1);
    }
}

__global__
void customKernelMul(int* a, int* b, int* r, int totalLen)
{
    int index = (blockDim.x * blockIdx.x) + threadIdx.x;

    //Prevent "overshoot"
    if (index >= totalLen)
    {
        return;
    }
    
    r[index] = a[index] * b[index];
    
}

void customCudaMul(int* a_host, int* b_host, int* r_host, int* a_device, int* b_device, int* r_device, int length)
{
    int numElements = length;
    //Not sure about threadsPerBlock. 1024 is the max so I set it to the max
    //Works well enough, so...
    int threadsPerBlock = 1024;
    int blocksPerGrid = (int)ceil(((float)numElements / (float)threadsPerBlock));
    const size_t workingSize = sizeof(int) * numElements;

    //------------------------------------------------------------------
    //--------------------------MEMCPY HOST TO DEVICE-------------------
    //------------------------------------------------------------------

    checkCudaError(cudaMemcpy(a_device, a_host, workingSize, cudaMemcpyHostToDevice));
    checkCudaError(cudaMemcpy(b_device, b_host, workingSize, cudaMemcpyHostToDevice));

    //------------------------------------------------------------------
    //--------------------------EXECUTE KERNEL--------------------------
    //------------------------------------------------------------------

    customKernelMul<<<blocksPerGrid, threadsPerBlock>>>(a_device, b_device, r_device, numElements);

    checkCudaError(cudaGetLastError());

    //------------------------------------------------------------------
    //-------------------COPY RESULT FROM DEVICE TO HOST----------------
    //------------------------------------------------------------------

    checkCudaError(cudaMemcpy(r_host, r_device, workingSize, cudaMemcpyDeviceToHost));

    //------------------------------------------------------------------
    //-----------------------DONE---------------------------------------
    //------------------------------------------------------------------
}
