#include <cuda_runtime.h>
#include "customCudaKernels.cuh"

#include <emutools/timer.h>

#include <stdlib.h>
#include <chrono>
#include <iostream>

using emutools::timer;

constexpr int batchSize{100'000'000};

void cuda()
{
    //Allocate all the memory that is going to be used beforehand
    //Host memory
    int* a_host = nullptr;
    int* b_host = nullptr;
    int* r_host = nullptr;
    //cudaHostAlloc is better than malloc or new because this way the memory will be page locked
    checkCudaError(cudaHostAlloc(&a_host, batchSize * sizeof(int), 0));
    checkCudaError(cudaHostAlloc(&b_host, batchSize * sizeof(int), 0));
    checkCudaError(cudaHostAlloc(&r_host, batchSize * sizeof(int), 0));

    for (int i = 0; i < batchSize; i++) 
    {
        a_host[i] = 3; //a contains 3,3,3,3,3,...
        b_host[i] = i; //b contains 1,2,3,4,5,...
    }

    int* a_device = nullptr;
    int* b_device = nullptr;
    int* r_device = nullptr;
    checkCudaError(cudaMalloc((void**)&a_device, batchSize * sizeof(int)));
    checkCudaError(cudaMalloc((void**)&b_device, batchSize * sizeof(int)));
    checkCudaError(cudaMalloc((void**)&r_device, batchSize * sizeof(int)));
    //All memory allocated

    timer timer; //Start timer

    //Call CUDA kernel
    customCudaMul(a_host, b_host, r_host, a_device, b_device, r_device, batchSize);
    //Results are now stored in r_host

    double elapsed{timer.elapsed()}; //Measure timer
    std::cout << "CUDA took " << elapsed << std::endl;

    //Done. Free memory.
    checkCudaError(cudaFreeHost((void*)a_host));
    checkCudaError(cudaFreeHost((void*)b_host));
    checkCudaError(cudaFreeHost((void*)r_host));
    checkCudaError(cudaFree((void*)a_device));
    checkCudaError(cudaFree((void*)b_device));
    checkCudaError(cudaFree((void*)r_device));
}

void cpu()
{
    int* a{new int[batchSize]{}};
    int* b{new int[batchSize]{}};
    int* r{new int[batchSize]{}};

    for (int i = 0; i < batchSize; i++) 
    {
        a[i] = 3; //a contains 3,3,3,3,3,...
        b[i] = i; //b contains 1,2,3,4,5,...
    }

    timer timer; //Start timer

    for (int i = 0; i < batchSize; i++) 
    {
        r[i] = a[i] * b[i];
    }

    double elapsed{timer.elapsed()}; //Measure timer
    std::cout << "CPU took " << elapsed << std::endl;

    delete[] a;
    delete[] b;
    delete[] r;
}

int main()
{
    cuda();
    cpu();
}