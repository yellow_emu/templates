#include "LedCommand.pb.h"
#include "RGBAData.pb.h"

#include <iostream>
#include <string>
#include <ios>

using std::cout;
using std::endl;

int main()
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    {
        emuproto::LEDCommand message{};
        message.set_turnon(true);
        message.set_led(emuproto::LED::YELLOW);

        std::string messageData{message.SerializeAsString()};

        emuproto::LEDCommand message2{};
        message2.ParseFromString(messageData);

        cout << message2.DebugString() << endl;
    }

    {
        emuproto::RGBAData image{};
        image.add_pixel(0xFF0000FF);
        image.add_pixel(0x00FF00FF);
        image.add_pixel(0x0000FFFF);

        std::string messageData{image.SerializeAsString()};

        emuproto::RGBAData image2{};
        image2.ParseFromString(messageData);

        auto size{image.pixel_size()};

        for(int i{0}; i < size; i++)
        {
            cout << image2.pixel(i) << endl;
        }
    }
}