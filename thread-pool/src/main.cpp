#include <BS_thread_pool_light.hpp>
#include <emutools/timer.h>
#include <thread>
#include <chrono>

#include <iostream>

using BS::thread_pool_light;
using emutools::timer;
using std::this_thread::sleep_for;
using std::chrono::seconds;
using std::cout;
using std::endl;

void waitOneSecond()
{
    sleep_for(seconds(1));
}

int main()
{
    cout << "starting task" << endl;

    timer timer{};
    for(int i = 0; i < 10; i++)
    {
        waitOneSecond();
    }
    double elapsed{ timer.elapsed() };
    cout << "without pool it took " << elapsed << endl;

    thread_pool_light pool{};
    timer.reset();
    pool.push_loop(10,
        [](const int a, const int b)
        {
            for(int i{ a }; i < b; i++)
            {
                waitOneSecond();
            }
        }
    );
    pool.wait_for_tasks();
    elapsed = timer.elapsed();
    cout << "with pool it took " << elapsed << endl;
}