#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>

int main()
{
    std::cout << "Start" << std::endl;
    
    int height{270}; //(Rows)
    int width{480}; //(Columns)
    
    cv::Mat image(height, width, CV_8UC3); //Image container class
    
    for(int row = 0; row < height; row++)
    {
        for(int col = 0; col < width; col++)
        {
            //BGR value: blue and red always 0, green depends on current column
            double colorvalue{(col/double(width))*255.0};
            cv::Vec3b color(0, 0, colorvalue);
            image.at<cv::Vec3b>(row,col) = color; //Set pixel in the image
        }
    }
    
    std::string filepath = "C:\\Users\\emu\\Desktop\\image.png";
    cv::imwrite(filepath, image); //Write image to disk
    
    std::cout << "Wrote image to " << filepath << std::endl;
    
    return EXIT_SUCCESS;
}
