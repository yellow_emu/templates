#include "LedCommand.pb.h"

#include <mqtt/async_client.h>

#include <iostream>
#include <string>
#include <climits>

using std::cout;
using std::endl;
using std::cerr;
using std::cin;

int main()
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    // std::string address{ "tcp://pi:1883" };
    std::string address{ "localhost" };

    cout << "Initializing for server '" << address << "'..." << endl;

    mqtt::async_client client(address, "olaf");

    try 
    {
        cout << "Connecting..." << endl;

        client.connect()->wait();

        cout << "Publishing messages..." << endl;

        mqtt::topic topic(client, "ledcommand", 1);

        while(true)
        {
            std::string result;
            std::cin >> result;

            ifproto::LEDCommand message{};

            if(result.compare("r") == 0)
            {
                static bool enable{ true };

                message.set_led(ifproto::LED::RED);
                message.set_turnon(enable);

                enable = !enable;
            }
            else if(result.compare("g") == 0)
            {
                static bool enable{ true };

                message.set_led(ifproto::LED::GREEN);
                message.set_turnon(enable);

                enable = !enable;
            }
            else if(result.compare("b") == 0)
            {
                static bool enable{ true };

                message.set_led(ifproto::LED::BLUE);
                message.set_turnon(enable);

                enable = !enable;
            }
            else if(result.compare("y") == 0)
            {
                static bool enable{ true };

                message.set_led(ifproto::LED::YELLOW);
                message.set_turnon(enable);

                enable = !enable;
            }
            else if(result.compare("w") == 0)
            {
                static bool enable{ true };

                message.set_led(ifproto::LED::WHITE);
                message.set_turnon(enable);

                enable = !enable;
            }
            else
            {
                break;
            }

            //Serialize data and send it
            std::string messageData{message.SerializeAsString()};
            topic.publish(messageData)->wait();

            cout << "OK" << endl;
        }

        cout << "Disconnecting..." << endl;
        client.disconnect()->wait();
    }
    catch (const mqtt::exception& e)
    {
        cerr << e << endl;
        return 1;
    }

     return 0;
}
