# qt-image
QT project showing how images can be created and shown with QImage and QLabel

QT has to be installed manually. Also set an environment variable "QTDIR" that points to your main installation path (or directly insert the path - check CMake files)

## Build with pure CMake:

`cmake -B build` <br>
`cmake --build build`

## Conan:

`conan install . --output-folder=build --build=missing`

to initialize conan and make it fetch all needed libraries. Then run

`cmake -B build --preset conan-default` <br>
`cmake --build --preset conan-release`

to configure and build the project.

To also have a debug configuration, use

`conan install . --output-folder=build --build=missing --settings=build_type=Debug`

and build with

`cmake --build --preset conan-debug`

