cmake_minimum_required(VERSION 3.22)

project(minimal LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

include(dependencies.cmake)

find_package(SFML REQUIRED COMPONENTS system window graphics network audio)

add_subdirectory(src)

add_executable(prog ${sourceFiles})
target_include_directories(prog PRIVATE ${includeDirs})

target_link_libraries(prog PRIVATE
    sfml-graphics
    sfml-window
    sfml-system
    emutools
)