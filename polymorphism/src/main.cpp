#include "Shape.hpp"
#include "Tri.hpp"
#include "Rec.hpp"

#include <memory>
#include <vector>

int main()
{
	std::vector<std::shared_ptr<Shape>> shapes
	{
		std::make_shared<Tri>(),
		std::make_shared<Rec>()
	};
	
	for(const auto& element : shapes)
	{
		element->print();
	}
}