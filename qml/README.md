# QMLExample
Project template for QML with CMake

To generate build files run `cmake -B build`
Then, to build, `run cmake --build build`

Set an environment variable "QTDIR" on your system, so CMake can find your QT installation.