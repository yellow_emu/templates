import QtQuick
import QtQuick.Controls

ApplicationWindow{
    id: root
    width: 640
    height: 480
    visible: true
    title: "Hello World"

    property var names: Backend.nameList
    property int selected : 0

    Row{
        y: 10
        spacing: 4
        CustomRect{}

        Column{
            x: 100
            spacing: 2

            Label{
                text: "Last added: " + Backend.testname
            }

            Row{
                spacing: 2
                TextField{
                    id: textFieldName
                    width: 100
                }
                SpinBox{
                    id: spinBoxAge
                    width: 40
                }
            }

            Button{
                text: "Add"
                onClicked:{
                    Backend.testname = textFieldName.text
                    Backend.addName(textFieldName.text, spinBoxAge.value);
                }
            }

            Button{
                text: "Remove selected"
                onClicked:{
                    let savedSelected = selected;
                    selected = 0;
                    Backend.removeName(savedSelected);
                }
            }
        }

        Column{
            spacing: 2
            Repeater{
                model: names
                delegate: Item{
                    width: rect.width
                    height: rect.height
                    Rectangle{
                        id: rect
                        width: 150
                        height: 30
                        radius: 5
                        color: "white"
                        opacity: root.selected === index ? 0.5 : 0.2
                    }
                    Label{
                        anchors.centerIn: parent
                        text: index + ": " + modelData.name + ", Age : " + modelData.age
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked:{
                            root.selected = index;
                        }
                    }
                }
            }
        }
    }
}
