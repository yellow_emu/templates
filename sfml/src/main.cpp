#include <emutools/random.h>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <vector>

constexpr int windowWidth{ 480 };
constexpr int windowHeight{ 270 };
constexpr float circleSize{ 20.0f };

int main()
{
    sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "SFML-Basic", sf::Style::Close);
    window.setFramerateLimit(60);

    emutools::random<int> rngColor(0, 255);
    emutools::random<int> rngPositionX(0, window.getSize().x);
    emutools::random<int> rngPositionY(0, window.getSize().y);

    std::vector<sf::CircleShape> objects;

    while(window.isOpen())
    {
        // 1) Poll events
        sf::Event event;
        while(window.pollEvent(event))
        {
            switch(event.type)
            {
                case sf::Event::Closed:
                {
                    window.close();
                    break;
                }

                case sf::Event::MouseButtonPressed:
                {
                    if(event.mouseButton.button == sf::Mouse::Left)
                    {
                        //Create object at point where mouse was clicked
                        sf::CircleShape object(circleSize);
                        object.setPosition(event.mouseButton.x - circleSize, event.mouseButton.y - circleSize);
                        object.setFillColor(sf::Color(rngColor.generate(), rngColor.generate(), rngColor.generate()));
                        objects.push_back(object);
                    }
                    break;
                }

                case sf::Event::KeyPressed:
                {
                    switch(event.key.code)
                    {
                        case sf::Keyboard::BackSpace:
                        {
                            //Erase all objects
                            objects.clear();
                            break;
                        }
                        case sf::Keyboard::G:
                        {
                            //Create object at random location
                            sf::CircleShape object(circleSize);
                            object.setPosition(rngPositionX.generate() - circleSize, rngPositionY.generate() - circleSize);
                            object.setFillColor(sf::Color(rngColor.generate(), rngColor.generate(), rngColor.generate()));
                            objects.push_back(object);
                            break;
                        }
                        case sf::Keyboard::R:
                        {
                            //Erase most recently created object
                            if(objects.size() > 0)
                            {
                                objects.erase(objects.end() - 1);
                            }

                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                    break;
                }

                default:
                {
                    break;
                }
            }
        }

        // 2) Draw to window
        window.clear();

        for(const auto& object : objects)
        {
            window.draw(object);
        }

        window.display();
    }

    return 0;
}
