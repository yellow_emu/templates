#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include "Backend.hpp"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    QQuickStyle::setStyle("Fusion");

    Backend backend{};
    engine.rootContext()->setContextProperty("Backend", &backend);
    
    const QUrl url(u"qrc:/qml/main.qml"_qs);
    
    QObject::connect
    (
        &engine, 
        &QQmlApplicationEngine::objectCreationFailed,
        &app, 
        []() { QCoreApplication::exit(-1); },
        Qt::QueuedConnection
    );
    
    engine.load(url);

    return app.exec();
}
