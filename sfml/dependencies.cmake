include(FetchContent)

set(FETCHCONTENT_QUIET FALSE)

######################################################
################  emutools ############################
######################################################

FetchContent_Declare(emutools
    GIT_REPOSITORY https://gitlab.com/yellow_emu/emutools.git
    GIT_TAG main
    GIT_PROGRESS TRUE
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
)

FetchContent_GetProperties(emutools)
if(NOT emutools_POPULATED)
    FetchContent_Populate(emutools)
endif()

add_library(emutools INTERFACE)
target_include_directories(emutools INTERFACE ${emutools_SOURCE_DIR})

######################################################
######################################################
######################################################
