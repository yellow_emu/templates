#include "Backend.hpp"

#include "NameEntry.hpp"

#include <iostream>

Backend::Backend()
    : QObject(nullptr)
{
    m_nameList.append(new NameEntry("Olaf", 30));
    m_nameList.append(new NameEntry("Günther", 31));
}

void Backend::centerCircleClicked(QString colorDesc)
{
    std::cout << "Center circle clicked! Color is now: " << colorDesc.toStdString() << std::endl;
}

void Backend::addName(QString name, int age)
{
    m_nameList.append(new NameEntry(name, age));
    emit nameListChanged();
}

void Backend::removeName(int index)
{
    if(m_nameList.size() > 1)
    {
        delete m_nameList.at(index);
        m_nameList.removeAt(index);
        emit nameListChanged();
    }
}
