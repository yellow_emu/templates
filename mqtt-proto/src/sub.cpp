#include "LedCommand.pb.h"

#include <mqtt/async_client.h>

#include <ios>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::cin;

class callback : public mqtt::callback
{
    void message_arrived(mqtt::const_message_ptr msg) override
    {
        cout << "Message received: " << endl;
        std::string data{ msg->to_string() };

        //Deserialize message and process it
        ifproto::LEDCommand message{};
        message.ParseFromString(data);

        cout << "LED: ";
        switch(message.led())
        {
            case ifproto::LED::RED:
            {
                cout << "red" << endl;
                break;
            }
            case ifproto::LED::GREEN:
            {
                cout << "green" << endl;
                break;
            }
            case ifproto::LED::BLUE:
            {
                cout << "blue" << endl;
                break;
            }
            case ifproto::LED::YELLOW:
            {
                cout << "yellow" << endl;
                break;
            }
            case ifproto::LED::WHITE:
            {
                cout << "white" << endl;
                break;
            }
            default:
            {
                cout << "impossible case" << endl;
                break;
            }
        }

        cout << std::boolalpha << "turnon: " << message.turnon() << std::noboolalpha << endl;
    }
};

int main()
{
    // std::string address{ "tcp://pi:1883" };
    std::string address{ "localhost" };

    cout << "Initializing for server '" << address << "'..." << endl;

    mqtt::async_client client(address, "gunther");

    callback cb;
    client.set_callback(cb);

    try 
    {
        cout << "Connecting..." << endl;
        client.connect()->wait();

        cout << "Subscribing.." << endl;;
        client.subscribe("ledcommand", 1)->wait();

        std::string result;
        std::cin >> result;

        client.disconnect()->wait();
        cout << "Disconnected" << endl;
    }
    catch (const mqtt::exception& e) 
    {
        cerr << e << endl;
        return 1;
    }

     return 0;
}
