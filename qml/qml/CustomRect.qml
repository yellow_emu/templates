import QtQuick

Rectangle{
    height: 60
    width: 60
    color: "purple"

    property bool isGreen: true
    
    CustomCircle{
        id: customCircle
        anchors.centerIn: parent
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(isGreen)
                {
                    customCircle.color = "blue";
                    isGreen = false;
                }
                else
                {
                    customCircle.color = "green";
                    isGreen = true;
                }

                Backend.centerCircleClicked(customCircle.color);
            }
        }
    }
}