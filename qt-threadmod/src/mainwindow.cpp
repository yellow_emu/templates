#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "engine/engine.h"

#include <iostream>
#include <thread>
#include <string>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setFixedSize(size());
    setWindowTitle("QTThreadModExample");
    connectSignalsToSlots();
    show();
}

//Task for thread
void task(MainWindow* window)
{
    window->setProgressLabelExt("Executing...");
    window->setProgressBarExt(0);

    engine::process(100,
        [window](int progress)->void
        {
            window->setProgressBarExt(progress);
        }
    );
    window->setButtonEnabledExt(true);
    window->setProgressLabelExt("Done");
}

void MainWindow::clickSlot()
{
    setButtonEnabled(false);
    std::thread t(task, this);
    t.detach();
}

//Native UI modification functions

void MainWindow::setProgressLabel(std::string text)
{
    ui->label->setText(QString::fromStdString(text));
}

void MainWindow::setProgressBar(int progress)
{
    ui->progressBar->setValue(progress);
}

void MainWindow::setButtonEnabled(bool enabled)
{
    ui->pushButton->setEnabled(enabled);
}

//Connect signals for external modification
void MainWindow::connectSignalsToSlots()
{
    QObject::connect(
        this,
        SIGNAL(SIGNAL_setProgressLabel(std::string)),
        this,
        SLOT(SLOT_setProgressLabel(std::string))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_setProgressBar(int)),
        this,
        SLOT(SLOT_setProgressBar(int))
    );

    QObject::connect(
        this,
        SIGNAL(SIGNAL_setButtonEnabled(bool)),
        this,
        SLOT(SLOT_setButtonEnabled(bool))
    );
}

MainWindow::~MainWindow()
{
    delete ui;
}

