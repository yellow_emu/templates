# mqtt-proto

Use protobuf to serialize messages and send them over MQTT

The Conan package for protobuf comes with the protoc compiler (in the bin folder).
Add that folder to your "path" environment variable so CMake can find the protoc command.

## Build with pure CMake:

`cmake -B build` <br>
`cmake --build build`

## Conan:

`conan install . --output-folder=build --build=missing`

to initialize conan and make it fetch all needed libraries. Then run

`cmake -B build --preset conan-default` <br>
`cmake --build --preset conan-release`

to configure and build the project.

To also have a debug configuration, use

`conan install . --output-folder=build --build=missing --settings=build_type=Debug`

and build with

`cmake --build --preset conan-debug`

