#include <mqtt/async_client.h>

#include <iostream>
#include <string>
#include <climits>

using std::cout;
using std::endl;
using std::cerr;
using std::cin;

class callback : public mqtt::callback
{
    void message_arrived(mqtt::const_message_ptr msg) override
    {
        cout << "Message received: ";
        std::string data{ msg->to_string() };
        cout << data << endl;
    }
};

int main()
{
    // std::string address{ "tcp://pi:1883" };
    std::string address{"localhost"};

    cout << "Initializing for server '" << address << "'..." << endl;

    mqtt::async_client client(address, "gunther");

    callback cb;
    client.set_callback(cb);

    try 
    {
        cout << "Connecting..." << endl;
        client.connect()->wait();

        cout << "Subscribing.." << endl;;
        client.subscribe("test", 1)->wait();

        std::string result;
        std::cin >> result;

        client.disconnect()->wait();
        cout << "Disconnected" << endl;
    }
    catch (const mqtt::exception& e) 
    {
        cerr << e << endl;
        return 1;
    }

     return 0;
}
