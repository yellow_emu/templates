#pragma once

#include <QMainWindow>
#include <string>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : private QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    //External modification functions
    //Other threads can use these functions to modify 
    //this window in a thread safe way

    void setProgressLabelExt(std::string text)
        {emit SIGNAL_setProgressLabel(text);}
    void setProgressBarExt(int progress)
        {emit SIGNAL_setProgressBar(progress);}
    void setButtonEnabledExt(bool enabled)
        {emit SIGNAL_setButtonEnabled(enabled);}

signals:
    void SIGNAL_setProgressLabel(std::string text);
    void SIGNAL_setProgressBar(int progress);
    void SIGNAL_setButtonEnabled(bool enabled);
    
private slots:
    void clickSlot();
    
    void SLOT_setProgressLabel(std::string text)
        {setProgressLabel(text);}
    void SLOT_setProgressBar(int progress)
        {setProgressBar(progress);}
    void SLOT_setButtonEnabled(bool enabled)
        {setButtonEnabled(enabled);}

private:
    Ui::MainWindow *ui;
    void connectSignalsToSlots();

    void setProgressLabel(std::string text);
    void setProgressBar(int progress);
    void setButtonEnabled(bool enabled);
};
