#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , counter{0}
{
    ui->setupUi(this);
    //Make non-resizable
    this->setFixedSize(this->size());
    this->setWindowTitle("QTMinimalExample");
    

    QPixmap pix(":/resources/icon.png");
    ui->labelIcon->setPixmap(pix.scaledToHeight(ui->labelIcon->width()));
}

void MainWindow::countSlot()
{
    counter++;
    QString s = QString::number(counter);
    ui->label->setText(s);
}

MainWindow::~MainWindow()
{
    delete ui;
}

