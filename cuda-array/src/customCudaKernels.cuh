#pragma once

void customCudaMul(int* a_host, int* b_host, int* r_host, int* a_device, int* b_device, int* r_device, int length);
void checkCudaError(cudaError_t error);