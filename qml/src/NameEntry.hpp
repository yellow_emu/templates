#pragma once

#include <QObject>

class NameEntry : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ getName    WRITE setName   NOTIFY nameChanged)
    Q_PROPERTY(int      age READ getAge     WRITE setAge    NOTIFY ageChanged)
public:
    NameEntry(QString name, int age)
    : m_name{name}
    , m_age{age}
    {}

    QString getName(){return m_name;}
    void setName(QString name){m_name = name; emit nameChanged();}

    int getAge(){return m_age;}
    void setAge(int age){m_age = age;}

signals:
    void nameChanged();
    void ageChanged();

private:
    QString m_name;
    int m_age;
};