#pragma once

#include "NameEntry.hpp"

#include <QObject>
#include <QString>
#include <QList>

class Backend : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString testname READ getTestname WRITE setTestName NOTIFY testnameChanged)
    Q_PROPERTY(QList<QObject*> nameList READ getNameList NOTIFY nameListChanged)

public:
    Backend();

    QString getTestname(){return m_testName;}
    void setTestName(QString testName){m_testName = testName; emit testnameChanged();}

    QList<QObject*> getNameList(){return m_nameList;}

    Q_INVOKABLE void centerCircleClicked(QString colorDesc);
    Q_INVOKABLE void addName(QString name, int age);
    Q_INVOKABLE void removeName(int index);

signals:
    void testnameChanged();
    void nameListChanged();

private:
    QString m_testName{"Olaf"};
    QList<QObject*> m_nameList{};
};