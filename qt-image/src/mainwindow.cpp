#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , counter{0}
{
    ui->setupUi(this);
    //Make non-resizable
    setFixedSize(this->size());
    setWindowTitle("QTImageExample");

    _image = new QImage(128, 128, QImage::Format_RGB888);

    int rows = _image->height();
    int cols = _image->width();

    for(int row = 0; row < rows; row++)
    {
        for(int col = 0; col < cols; col++)
        {
            QColor color;
            color.setRgbF(0.0f, row / float(rows), 0.0f);
            _image->setPixelColor(col, row, color);
        }
    }

    _image->save("image.png");

    ui->labelImage->setPixmap(QPixmap::fromImage(*_image));
    
}

void MainWindow::countSlot()
{
    counter++;

    if(counter == 25)
    {
        counter = 0;
    }
    
    QString s = QString::number(counter);
    ui->label->setText(s);

    int rows = _image->height();
    int cols = _image->width();

    for(int row = 0; row < rows; row++)
    {
        for(int col = 0; col < cols; col++)
        {
            _image->setPixelColor(col, row, QColor(0, counter * 10, 0));
        }
    }

    _image->save("image.png");

    ui->labelImage->setPixmap(QPixmap::fromImage(*_image));
}

MainWindow::~MainWindow()
{
    delete ui;
}

