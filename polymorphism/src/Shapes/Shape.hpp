#pragma once

class Shape
{
public:
	virtual void print() = 0;
};