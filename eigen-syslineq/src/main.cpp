#include <Eigen/Dense>

#include <iostream>

using std::cout;
using std::endl;

int main()
{
    /**
     *  Let's say we want to calcuate the intersection of a plane with a line.
     *  The plane is defined as e: (0,1,0) + a*(1,1,0) + b*(0,0,1)
     *  The line is defined as g: (0,2,1) + c*(1,0,0)
     *  
     *  We can get the solution by creating an equation with each definition on one side and solving for a, b and c
     *  Afterwards we can just calculate the expression on any side of that equation to get the intersection point.
     * 
     *  To coumpute this in a program, we have to first create the linear system of equations with matrices in a form of
     *  AX = B where X are the paramaters a, b and c we are looking for. 
     * 
     *  Starting equations:
     * 
     *  es_x + a * ea_x + b * eb_x = gs_x + c * gc_x
     *  es_y + a * ea_y + b * eb_y = gs_y + c * gc_y
     *  es_z + a * ea_z + b * eb_z = gs_z + c * gc_z
     * 
     *  After re-order:
     * 
     *  a * ea_x + b * eb_x - c * gc_x = gs_x - es_x
     *  a * ea_y + b * eb_y - c * gc_y = gs_y - es_y
     *  a * ea_z + b * eb_z - c * gc_z = gs_z - es_z
     * 
     *  In matrix form:
     * 
     *  ( ea_x eb_x -gc_x )    ( a )   ( gs_x - es_x )
     *  ( ea_y eb_y -gc_y ) *  ( b ) = ( gs_y - es_y )
     *  ( ea_z eb_z -gc_z )    ( c )   ( gs_z - es_z )
     * 
     * We can then solve X by calculating:
     * 
     *  X = (A^-1)B
     * 
     *  then we resolve either side of the function to get the solution. It should resolve to a = 1, b = 1, c = 1
     *  which leads to an intersection point of i: (1,2,1)
     * 
     */

    //Define plane
    Eigen::Vector3d e_start{ 
        0.0, 
        1.0, 
        0.0 
    };

    Eigen::Vector3d e_a{ 
        1.0, 
        1.0, 
        0.0 
    };

    Eigen::Vector3d e_b{ 
        0.0, 
        0.0, 
        1.0 
    };

    //Define line
    Eigen::Vector3d g_start{ 
        0.0, 
        2.0, 
        1.0 
    };

    Eigen::Vector3d g_c{ 
        1.0, 
        0.0, 
        0.0 
    };

    //Create system of equations

    //Define A
    Eigen::Matrix3d A_m{
        {e_a.x(), e_b.x(), -g_c.x()},
        {e_a.y(), e_b.y(), -g_c.y()},
        {e_a.z(), e_b.z(), -g_c.z()}
    };

    //Define B
    Eigen::Vector3d B_m{
        g_start.x() - e_start.x(),
        g_start.y() - e_start.y(),
        g_start.z() - e_start.z()
    };

    //Our result paramaters are defined as X = (A^-1)B
    Eigen::Vector3d resultParamaters{ A_m.inverse() * B_m };

    cout << "Result paramaters: "
        "a: " << resultParamaters.x() << ", " 
        "b: " << resultParamaters.y() << ", " 
        "c: " << resultParamaters.z() 
    << endl;

    //Calculate intersection point with g:
    double c{ resultParamaters.z() };
    Eigen::Vector3d intersection{ g_start + c * g_c };

    cout << "Intersection point: "
        << intersection.x() << ", " 
        << intersection.y() << ", " 
        << intersection.z() 
    << endl;
}
