#include <mqtt/async_client.h>

#include <iostream>
#include <string>
#include <climits>

using std::cout;
using std::endl;
using std::cerr;
using std::cin;

int main()
{
    
    // std::string address{ "tcp://pi:1883" };
    std::string address{"localhost"};

    cout << "Initializing for server '" << address << "'..." << endl;

    mqtt::async_client client(address, "olaf");

    try 
    {
        cout << "Connecting..." << endl;

        client.connect()->wait();

        cout << "Publishing messages..." << endl;

        mqtt::topic topic(client, "test", 1);

        while(true)
        {
            std::string result;
            std::cin >> result;

            if(result.compare("q") == 0)
            {
                break;
            }

            topic.publish(result)->wait();

            cout << "OK" << endl;
        }

        cout << "Disconnecting..." << endl;
        client.disconnect()->wait();
    }
    catch (const mqtt::exception& e)
    {
        cerr << e << endl;
        return 1;
    }

     return 0;
}
